nop
nop
nop # Initialize Values
addi $1, $0, 4		# r1 = 4
addi $2, $0, 5		# r2 = 5
sub $3, $0, $1		# r3 = -4
sub $4, $0, $2		# r4 = -5
nop
nop
nop # Basic Test of BLT
blt $1, $2, b3		# r1 < r2 --> taken
nop				# flushed instruction
nop				# flushed instruction
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
b3: addi $10, $10, 1		# r10 += 1 (Correct)
blt $2, $2, b4		# r2 == r2 --> not taken
nop				# nop in case of flush
nop				# nop in case of flush
nop				# Spacer
addi $10, $10, 1			# r10 += 1 (Correct)
b4: nop				# Landing pad for branch
blt $4, $1, b5			# r4 < r1 --> taken
nop				# flushed instruction
nop				# flushed instruction
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
b5: addi $10, $10, 1		# r10 += 1 (Correct)        //broken here
# blt $2, $1, b6		# r2 > r1 --> not taken
# nop				# nop in case of flush
# nop				# nop in case of flush
# nop				# Spacer
# addi $10, $10, 1		# r10 += 1 (Correct)
# b6: nop				# Landing pad for branch
# blt $4, $3, b7		# r4 < r3 --> taken
# nop				# flushed instruction
# nop				# flushed instruction
# addi $20, $20, 1		# r20 += 1 (Incorrect)
# addi $20, $20, 1		# r20 += 1 (Incorrect)
# addi $20, $20, 1		# r20 += 1 (Incorrect)
# b7: addi $10, $10, 1		# r10 += 1 (Correct)
# blt $3, $4, b8		# r3 > r4 --> not taken
# nop				# nop in case of flush
# nop				# nop in case of flush
# nop				# Spacer
# addi $10, $10, 1		# r10 += 1 (Correct)
# b8: nop				# Landing pad for branch
# nop				# Avoid add RAW hazard
# nop				# Avoid add RAW hazard
# add $11, $10, $11		# Accumulate r10 score
# add $21, $20, $21		# Accumulate r20 score
# and $10, $0, $10		# r10 should be 6
# and $20, $0, $20		# r20 should be 0
# nop