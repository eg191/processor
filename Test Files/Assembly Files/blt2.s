nop
nop
nop # Initialize Values
addi $1, $0, 4		# r1 = 4
addi $2, $0, 5		# r2 = 5
sub $3, $0, $1		# r3 = -4
sub $4, $0, $2		# r4 = -5
nop
nop
nop
blt $4, $1, b5			# r4 < r1 --> taken
nop				# flushed instruction
nop				# flushed instruction
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
b5: addi $10, $10, 1		# r10 += 1 (Correct) 