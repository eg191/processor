nop # bypassing and memory
nop # Values initialized using addi (positive only)
nop # Author: Elaine Guo
nop
nop
nop 
addi $0, $0, 0
addi $1, $1, 1
addi $2, $2, 2
addi $4, $4, 4
addi $5, $5, 5
addi $6, $6, 6
addi $7, $7, 7
addi $8, $8, 8
addi $9, $9, 9
addi $10, $10, 10
add $3, $2, $1 #should be 3
add $4, $3, $3 #should be 6
sw $4, 0($4)
lw $11, 0($4) #should be 6
sw $11, 0($1)
lw $12, 0($1) #should be 6
sw $3, 0($3)
lw $16, 0($3) #should be 3
addi $19, $19, -1
sw $19, 0($10)
lw $18, 0($10) #should be -1
addi $17, $17, -4398
add $12, $2, $11
sw $7, 0($7)
add $21, $7, $7
sw $21, 0($21)
lw $22, 0($21)
add $24, $2, $21
sw $24, 0($9)
add $25, $24, $1
add $26, $1, $1
nop
add $27, $26, $26
add $0, $2, $1
add $30, $0, $0
