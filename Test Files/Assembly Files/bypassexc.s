nop             # Values initialized using addi (positive only)
nop             # Author: Oliver Rodas
nop
nop
nop             # Exception Bypassing
addi $1, $0, 32767    # r1 = 32767
sll $1, $1, 16        # r1 = 2147418112
addi $1, $1, 65535    # r1 = 2147483647 (Max positive integer)
addi $2, $0, 2        # r2 = 2
addi $3, $0, 1        # r3 = 1
add $5, $1, $3        # add ovfl --> rstatus = 1
add $4, $2, $30        # r4 = r2 + rstatus = 3    (X->D)