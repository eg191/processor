nop # Basic Control Test with no Data Hazards
nop # Values initialized using addi (positive only) and sub
nop # Registers 10,11 track correct and 20,21 track incorrect
nop # Values and comments in the test of JR must be updated if code modified.
nop # Author: Nathaniel Brooke
nop
nop
nop # Initialize Values
addi $1, $0, 4		# r1 = 4
addi $2, $0, 5		# r2 = 5
sub $3, $0, $1		# r3 = -4
sub $4, $0, $2		# r4 = -5
nop
nop 				# Basic Test of BNE
bne $1, $2, b1		# r1 != r2 --> taken
nop				# flushed instruction
nop				# flushed instruction
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
addi $20, $20, 1		# r20 += 1 (Incorrect)
b1: addi $10, $10, 1		# r10 += 1 (Correct)
bne $2, $2, b2		# r2 == r2 --> not taken
nop				# nop in case of flush
nop				# nop in case of flush
nop				# Spacer
addi $10, $10, 1		# r10 += 1 (Correct)
b2: nop				# Landing pad for branch
nop				# Avoid add RAW hazard
# add $11, $10, $11		# Accumulate r10 score
# add $21, $20, $21		# Accumulate r20 score
# # and $10, $0, $10		# r10 should be 2
# # and $20, $0, $20		# r20 should be 0