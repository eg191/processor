nop # Basic Test of jr
addi $1, $0, 4		# r1 = 4
addi $2, $0, 5		# r2 = 5
sub $3, $0, $1		# r3 = -4
sub $4, $0, $2		# r4 = -5
nop 	
addi	$31, $0, 11	# $r31 = 18
jr	$31		# go to j2
addi 	$20, $20, 1	# r20 += 1 (Incorrect)
addi 	$20, $20, 1	# r20 += 1 (Incorrect)
addi 	$20, $20, 1	# r20 += 1 (Incorrect)
j2:
addi	$10, $10, 1	# r10 += 1 (Correct)
nop
nop
nop
nop
# Final: $r10 should be 1, $r20 should be 0