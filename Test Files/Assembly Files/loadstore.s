nop # Basic lw/sw instruction tests 
nop # Values initialized using addi (positive only)
nop # Author: Guo, Elaine
nop
nop
nop 
addi $0, $0, 0
addi $1, $1, 1
addi $2, $2, 2
addi $3, $3, 3
addi $4, $4, 4
addi $5, $5, 5
addi $6, $6, 6
addi $7, $7, 7
addi $8, $8, 8
addi $9, $9, 9
addi $10, $10, 10
nop
nop
nop
sw $10, 3($3)
nop
nop
nop
nop
lw $11, 3($3)
nop