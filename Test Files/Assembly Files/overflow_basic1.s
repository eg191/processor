
nop # Basic overflow Test with no Hazards
nop # Values initialized using addi (positive only)
nop # Author: Oliver, Rodas
nop
nop
nop 
addi $0, $0, 0
addi $1, $1, 1
addi $2, $2, 2
addi $3, $3, 3
addi $4, $4, 4
addi $5, $5, 5
addi $6, $6, 6
addi $7, $7, 7
addi $8, $8, 8
addi $9, $9, 9
addi $10, $10, 10
sub $11, $11, 2147483648
nop
nop
nop
nop
sub $11, $11, 2147483647
nop
nop
nop
nop
