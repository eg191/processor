nop # Basic R-type instruction tests (w/0 multdiv)
nop # Values initialized using addi (positive only)
nop # Author: Guo, Elaine
nop
nop
nop 
addi $0, $0, 0
addi $1, $1, 1
addi $2, $2, 2
addi $3, $3, 3
addi $4, $4, 4
addi $5, $5, 5
addi $6, $6, 6
addi $7, $7, 7
addi $8, $8, 8
addi $9, $9, 9
addi $10, $10, 10
nop
nop
nop
add $1, $1, $2
sub $2, $2, $2
and $3, $3, $4
or $4, $4, $5
sll $5, $5, 2
sra $6, $6, 1