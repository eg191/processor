Cycle  12: Wrote 1 into register 1
Cycle  13: Wrote 2 into register 2
Cycle  14: Wrote 3 into register 3
Cycle  15: Wrote 4 into register 4
Cycle  16: Wrote 5 into register 5
Cycle  17: Wrote 6 into register 6
Cycle  18: Wrote 7 into register 7
Cycle  19: Wrote 8 into register 8
Cycle  20: Wrote 9 into register 9
Cycle  21: Wrote 10 into register 10
Cycle  22: Wrote 65535 into register 11
Cycle  27: Wrote 131070 into register 11
Cycle  32: Wrote 196605 into register 11
============== Testing Mode ==============
Reg  0:           0
Reg  1:           1
Reg  2:           2
Reg  3:           3
Reg  4:           4
Reg  5:           5
Reg  6:           6
Reg  7:           7
Reg  8:           8
Reg  9:           9
Reg 10:          10
Reg 11:      196605
Reg 12:           0
Reg 13:           0
Reg 14:           0
Reg 15:           0
Reg 16:           0
Reg 17:           0
Reg 18:           0
Reg 19:           0
Reg 20:           0
Reg 21:           0
Reg 22:           0
Reg 23:           0
Reg 24:           0
Reg 25:           0
Reg 26:           0
Reg 27:           0
Reg 28:           0
Reg 29:           0
Reg 30:           0
Reg 31:           0
